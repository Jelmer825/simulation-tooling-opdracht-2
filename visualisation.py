import urllib.request
import json
import matplotlib.pyplot as plt
from matplotlib import animation
import cartopy.crs as ccrs
from cartopy.io.img_tiles import OSM
import numpy as np
import random
from multiprocessing import Process,Queue,Pipe

class Visualisation(object):
    fig, ax = plt.subplots(figsize=(10,6))
    # Create a open street map
    osm = OSM()
    # Add projection to the axes
    ax = plt.axes(projection=ccrs.PlateCarree())
    # Show dusseldorf airport on the map with its coordinates
    ax.set_extent((6.73469, 6.80031, 51.27150, 51.30445))
    ax.set_title('Düsseldorf Airport')

    # Add the OSM image layer and zoom so the names wont be visible
    ax.add_image(osm, 15) 

    # Display all terminals
    ax.plot([6.76661, 6.76233, 6.76203], [51.28203, 51.27988, 51.27664], 'ro')

    # Plot with all plane locations, first list contains the longitude, second list contains the latitude
    planes_plot, = ax.plot([], [], 'bo')

    opener = urllib.request.build_opener()
    opener.addheaders = [('User-agent', 'Mozilla/5.0')]

    # Generate 100 evenly spaced steps between start and end of the landing strips, we will use this to plot the planes landing
    firstlandingstrip_cords = list(map(lambda x,y: [x,y], np.linspace(51.28209, 51.29989, num=100), np.linspace(6.74552, 6.78275, num=100)))
    firstlandingstrip_cords_reverse = firstlandingstrip_cords[::-1]
    secondlandingstrip_cords = list(map(lambda x,y: [x,y], np.linspace(51.27845, 51.29613, num=100), np.linspace(6.74981, 6.78688, num=100)))
    secondlandingstrip_cords_reverse = secondlandingstrip_cords[::-1]
    taxispots_cords = list(map(lambda x,y: [x,y], np.linspace(51.27833, 51.29319, num=100), np.linspace(6.75393, 6.78504, num=10)))

    planes = {}

    terminals = {
        'terminal1': {
            'occupied' : False,
            'lat': 51.28203,
            'long': 6.76661,
            'plane': ''
        },
        'terminal2': {
            'occupied' : False,
            'lat': 51.27988,
            'long': 6.76233,
            'plane': ''
        },
        'terminal3': {
            'occupied' : False,
            'lat': 51.27664,
            'long': 6.76203,
            'plane': ''
        }
    }

    landingstrips = {
        'strip1' : {
            'occupied' : False,
            'landing_cords' : firstlandingstrip_cords,
            'takeoff_cords' : firstlandingstrip_cords_reverse,
            'plane' : ''
        },
        'strip2' : {
            'occupied' : False,
            'landing_cords' : secondlandingstrip_cords,
            'takeoff_cords' : secondlandingstrip_cords_reverse,
            'plane' : ''
        }
    }

    def read_json(self):
        # Read the json file
        with open('result.json') as f:
            text = f.read()
            f.close()
            self.planes.update(json.loads(text))

    def write_csv(self, planes_dict):
        # Write the planes to a csv file
        with open('result.json', 'w') as fp:
            json.dump(planes_dict, fp)
    
    def assign_terminal(self, plane_id):
        # Assign terminal to plane
        for key, value in self.terminals.items():
            if not self.terminals[key]['occupied']:
                self.terminals[key]['occupied'] = True
                self.terminals[key]['plane'] = plane_id
                print(f'assigned terminal {key} to {plane_id}')
                break
    
    def clear_terminal(self, plane_id):
        # Clear a terminal
        terminal = self.get_terminal(plane_id)
        if terminal:
            self.terminals[terminal]['plane'] = ''
            self.terminals[terminal]['occupied'] = False
            print(f'de_assigned terminal {terminal} to {plane_id}')
    
    def assign_runway(self, plane_id):
        # Assign runway to plane
        for key, value in self.landingstrips.items():
            if not self.landingstrips[key]['occupied'] and not self.landingstrips[key]['plane']:
                self.landingstrips[key]['occupied'] = True
                self.landingstrips[key]['plane'] = plane_id
                print(f'assignedrunway {key} to {plane_id}')
                break

    def clear_runway(self, plane_id):
        # Clear a runway
        runway = self.get_runway(plane_id)
        if runway:
            self.landingstrips[runway]['plane'] = ''
            self.landingstrips[runway]['occupied'] = False
            print(f'de_assigned runway {runway} to {plane_id}')
    
    def get_runway(self, plane_id):
        # Get the runway the plane is currently in
        for key, value in self.landingstrips.items():
            if self.landingstrips[key]['plane'] == plane_id:
                return key

    def check_runway(self, plane_id):
        # Check if runway is occupied
        if self.planes[plane_id]['in_strip']:
            self.assign_runway(plane_id)
            return True
        else:
            self.clear_runway(plane_id)
            return False
    
    def get_terminal(self, plane_id):
        # Get the terminal the plane is currently in
        for key, value in self.terminals.items():
            if self.terminals[key]['plane'] == plane_id:
                return key

    def check_terminal(self, plane_id):
        # Check if the plane is in a terminal
        if self.planes[plane_id]['in_terminal']:
            self.assign_terminal(plane_id)
            return True
        else:
            self.clear_terminal(plane_id)
            return False

    def update_plane(self, plane_id, lat, long, step):
        # Update the plane location
        self.planes[plane_id]['lat'] = lat
        self.planes[plane_id]['long'] = long
        self.planes[plane_id]['step'] = step

        self.write_csv(self.planes)

    def animate(self, i):
        # Animate the planes
        self.read_json()
        # print(self.planes)
        
        # Clear all lists so we can plot the new positions and delete the old ones
        lat_list=[]
        long_list=[]

        # Update all planes
        for plane_key, plane_value in self.planes.items():
            if self.planes[plane_key]['state'] == "landing":
                runway = self.get_runway(plane_key)
                if not runway:
                    self.assign_runway(plane_key)
                if plane_value['step'] == 99:

                    break
                if runway:
                    lat = self.landingstrips[runway]['landing_cords'][plane_value['step']][0]
                    lon = self.landingstrips[runway]['landing_cords'][plane_value['step']][1]
                    self.update_plane(plane_key, lat, lon, plane_value['step'] + 1)

            if self.planes[plane_key]['state'] == "takeoff":
                self.clear_terminal(plane_key)
                runway = self.get_runway(plane_key)
                if not runway:
                    self.assign_runway(plane_key)
                if plane_value['step'] == 99:
                    break
                if runway:
                    lat = self.landingstrips[runway]['takeoff_cords'][plane_value['step']][0]
                    lon = self.landingstrips[runway]['takeoff_cords'][plane_value['step']][1]
                    self.update_plane(plane_key, lat, lon, plane_value['step'] + 1)

            if self.planes[plane_key]['state'] == "in_terminal":
                terminal = self.get_terminal(plane_key)
                if not terminal:
                    self.clear_runway(plane_key)
                    self.assign_terminal(plane_key)

                else:
                    lat = self.terminals[terminal]['lat']
                    lon = self.terminals[terminal]['long']
                    self.update_plane(plane_key, lat, lon, 1)

            if self.planes[plane_key]['state'] == "gone":
                self.clear_runway(plane_key)
                self.update_plane(plane_key, 0, 0, 0)
                
            lat_list.append(self.planes[plane_key]['lat'])
            long_list.append(self.planes[plane_key]['long'])

            # Set the data for the new plot, this will update the plot
            self.planes_plot.set_data(long_list, lat_list)
        return self.planes_plot,

    def main(self):  
        # Main function
        anim = animation.FuncAnimation(self.fig, self.animate, interval=100, blit=True)
        plt.show()

if __name__ == '__main__':
    # Create the plot
    visualisation = Visualisation()
    visualisation.main()