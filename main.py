
from random import randint, seed, choice
import simpy
import time
import zmq
import json


RANDOM_SEED = 42

LAND_TIME = 13
TAKEOFF_TIME = 13
ASSIGN_TIME = 1
BOARDING_TIME = 15
UNLOAD_TIME = 15
LOADING_TIME = 15
FUELING_TIME = 15
FLY_TIME = 15

def return_slots(res):
    return res.capacity

class Airport(object):
    """The airport model. It keeps track of all planes and is the place"""
    def __init__(self, env, name, runways, terminals):
        self.env = env

        self.name = name

        self.runway = simpy.Resource(env, capacity=runways)

        self.terminal = simpy.Resource(env, capacity=terminals)

    def write_csv(self, planes_dict):
        with open('result.json', 'w') as fp:
            json.dump(planes_dict, fp)

    def read_json(self):
        with open('result.json') as f:
            text = f.read()
            f.close()
            return json.loads(text)

    def flying(self, plane, away, runway_request=None):
        if not away:
            print(f"{plane} is flying", self.env.now)
            yield self.env.timeout(20)
            with self.terminal.request() as terminal_req:
                yield terminal_req
                with self.runway.request() as runway_req:
                    yield runway_req
                    yield self.env.process(self.landing(plane, runway_req, terminal_req))
        else:
            planes_dict = self.read_json()
            planes_dict[plane]["state"] = "gone"
            planes_dict[plane]["in_strip"] = False
            planes_dict[plane]["in_terminal"] = False
            self.write_csv(planes_dict)
            yield self.runway.release(runway_request)
            print(f"{plane} is flying away to never come back ...", self.env.now)

    def landing(self, plane, runway_request,terminal_request):
        print(f"{plane} is landing", self.env.now)
        planes_dict = self.read_json()
        planes_dict[plane]["state"] = "landing"
        planes_dict[plane]["in_strip"] = True
        self.write_csv(planes_dict)
        yield self.env.timeout(LAND_TIME)
        yield self.env.process(self.inTerminal(plane,runway_request,terminal_request))

    def inTerminal(self, plane,runway_request,terminal_request):
        self.runway.release(runway_request)
        planes_dict = self.read_json()
        planes_dict[plane]["state"] = "in_terminal"
        planes_dict[plane]["in_strip"] = False
        planes_dict[plane]["in_terminal"] = True
        self.write_csv(planes_dict)
        print(f"{plane} is in terminal", self.env.now)
        yield self.env.process(self.unloading(plane, terminal_request))

    def unloading(self, plane, terminal_request):
        print(f"{plane} is unloading", self.env.now)
        yield self.env.timeout(UNLOAD_TIME)
        yield self.env.process(self.fueling(plane, terminal_request))

    def fueling(self, plane, terminal_request):
        print(f"{plane} is fueling", self.env.now)
        yield self.env.timeout(FUELING_TIME)
        yield self.env.process(self.loading(plane, terminal_request))

    def loading(self, plane, terminal_request):
        print(f"{plane} is loading", self.env.now)
        yield self.env.timeout(LOADING_TIME)
        yield self.env.process(self.board(plane, terminal_request))

    def board(self, plane, terminal_request):
        print(f"{plane} is boarding", self.env.now)
        yield self.env.timeout(BOARDING_TIME)
        with self.runway.request() as runway_req:
            yield runway_req
            yield self.env.process(self.takeOff(plane, runway_req, terminal_request))

    def takeOff(self, plane, runway_request,terminal_request):
        self.terminal.release(terminal_request)
        planes_dict = self.read_json()
        planes_dict[plane]["state"] = "takeoff"
        planes_dict[plane]["in_strip"] = True
        planes_dict[plane]["in_terminal"] = False
        self.write_csv(planes_dict)
        print(f"{plane} is taking off!", self.env.now)
        yield self.env.timeout(TAKEOFF_TIME)
        yield self.env.process(self.flying(plane, True, runway_request))

def plane(env, name, airport):
    """The plane process (each plane has a ``name``) arrives at the airport
    and requests a runway and terminal. If that is possible land.
    It then starts the departion process, waits for it to finish and
    leaves to never come back ...
    """
    yield env.process(airport.flying(name, False))

def setup(env, initial_planes, airport):
    print("Setting up the airport")
    """Create an airport, a number of initial planes and keep creating planes
    approx. every ``t_inter`` seconds."""

    plane_list = ['Airbus A330-300','Airbus A340-300','Airbus A340-500',
    'Airbus A350-900','Boeing 777-200','Airbus A340-600',
    'Boeing 747-400','Boeing 747-8','Airbus A380-800']

    planes_dict = {}
    planes = []
    
    for x in range(0, initial_planes):
        plane_name = f"{choice(plane_list)}_{x}"
        planes_dict[plane_name] = {
            'lat': 51.28000,
            'long': 6.75000,
            'step': 0,
            'state': 'flying',
            'in_strip' : False,
            'in_terminal' : False
        }
    
    for key in planes_dict:
        planes.append(env.process(plane(env, key, airport)))

    with open('result.json', 'w') as fp:
            json.dump(planes_dict, fp)

    for x in range(len(planes)-1):
        randIndex = randint(0, len(planes)-1)
        yield planes[randIndex]
        planes.remove(planes[randIndex])

seed(RANDOM_SEED)

SIM_TIME = 500000
# env = simpy.Environment()
env = simpy.rt.RealtimeEnvironment(factor=1, strict=False)
airport = Airport(env, "Dusseldorf", 2, 3)
env.process(setup(env, 10, airport))

def run():
    time.sleep(5)
    env.run(until=SIM_TIME)

if __name__ == '__main__':
    run()